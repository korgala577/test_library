def findLambdas(file) {
    def lambdas = ""
    file.eachLine { line ->
        if (line.contains("code_s3")) {
            // Extracting the value between quotes assuming it follows the pattern "key = "value""
            def value = line.replaceAll(/.*= "(.*)"/, '$1')
            lambdas += '\n- ' +  value.split('/').last().tokenize('-')[0..2].join('-').replaceAll('.zip', '')

        }
    }
    return lambdas
}