def stage(message) {
    echoColor('blue', "STAGE: ${message}")
}

def description(message) {
    echoColor('blue', "${message}")
}
def info(message) {
    echoColor('blue', "INFO: ${message}")
}

def info_success(message) {
    echoColor('green', "INFO: ${message}")
}

def warning(message) {
    echoColor('red', "WARNING: ${message}")
}

def echoColor(color, message) {
    def colorMap = [
        'default': '',
        'red': '\033[31m',
        'blue': '\033[34m',
        'green': '\033[32m',
        'reset': '\033[0m'
    ]
    echo "${colorMap[color]}${message}${colorMap['reset']}"
}